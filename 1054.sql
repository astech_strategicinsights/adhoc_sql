select distinct  b.locationid as LocationId,
                  lc.name as ShopName,
                  a.friendlyname as FriendlyName,
                  b.address1 as Address,
                  b.city as ShopCity,
                  b.zipcode as ShopZip,
                  b.Country as ShopCountry,
                  b.isactive as Active_Flag,
                  --a.statusid as ,
                  lkp.text as ToolStatus,
                  a.serialnumber as SerialNumber,
                  a.IsTool
     from dbo.device a
            left outer join dbo.address b
                 on a.AssignedTo = b.LocationId
            join dbo.lookup lkp
                  on lkp.id = a.statusid
            join dbo.location lc
                on b.LocationId=lc.id
            join dbo.workorder w
                   on w.LocationId=b.locationid
              where b.country not in ('United States','Australia')
              and a.istool='false'
              and a.FriendlyName not like '%testing%'
              and a.statusid in (63)

order by  b.locationid,
          lc.name,
                    a.FriendlyName asc

